_以下為 myTomcat VM (兩台都要做 )_
### 安裝 openjdk
**各自進到 CentOS_demo 目錄下，SSH 連線 myTomcat16 及 myTomcat17** (文件以 myTomcat16 操作示範)

```
PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant ssh myTomcat16
```

將掛進來的 tar.gz 檔解壓到 /tmp/ 資料夾下
```
[vagrant@Tomcat16 ~]$ tar xvzf /vagrant_data/openjdk-11+28_linux-x64_bin.tar.gz -C /tmp/
```

將 /jdk-11/ 移到 /usr/local/ 下
```
[vagrant@Tomcat16 ~]$ sudo mv /tmp/jdk-11/ /usr/local/
```
在 /etc/profile 設定 java 環境變數
```
[vagrant@Tomcat16 ~]$ sudo vim /etc/profile
```
> 加上  
**<span style="color: blue;">export JAVA_HOME=/usr/local/jdk-11</span>**<br>
**<span style="color: blue;">export PATH=$JAVA_HOME/bin:$PATH</span>**

在父程序執行設定檔，讓環境變數立即生效
```
[vagrant@Tomcat16 ~]$ source /etc/profile
```

檢查 java 版本
```
[vagrant@Tomcat16 ~]$ java -version
```

<br>

### 安裝 tomcat

將掛進來的 tar.gz 檔解壓到 /tmp/ 資料夾下
```
[vagrant@Tomcat16 ~]$ tar xvzf /vagrant_data/apache-tomcat-9.0.39.tar.gz -C /tmp/
```

將 apache-tomcat-9.0.39 移到 /usr/local/ 下
```
[vagrant@Tomcat16 ~]$ sudo mv /tmp/apache-tomcat-9.0.39/ /usr/local/
```

在 /etc/profile 設定 tomcat 環境變數
```
[vagrant@Tomcat16 ~]$ sudo vim /etc/profile
```
> 加上  
**<span style="color: blue;">export CATALINA_HOME=/usr/local/apache-tomcat-9.0.39</span>**

在父程序執行設定檔，讓環境變數立即生效
```
[vagrant@Tomcat16 ~]$ source /etc/profile
```

啟動 tomcat
```
[vagrant@Tomcat16 ~]$ cd /usr/local/apache-tomcat-9.0.39/bin/
[vagrant@Tomcat16 bin]$ sh startup.sh 
```
> 以下指令可查看 tcp 協定的 port 監聽狀態  
[vagrant@Tomcat bin]$ sudo netstat -nlpt<br><br>_8080 --默認的 http 監聽 port_  
_8005 --用來關閉 tomcat 服務的 port_

測試
```
[vagrant@Tomcat16 bin]$ curl localhost:8080
或
[vagrant@Tomcat16 bin]$ curl 192.168.56.16:8080
```

將防火牆的對應端口打開
```
[vagrant@Tomcat16 bin]$ sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp
[vagrant@Tomcat16 bin]$ sudo firewall-cmd --reload
```
用 ip 192.168.56.16:8080 在 windows 主機測試 http://192.168.56.16:8080  ，若成功會看到以下頁面  

<img src="./images/image-2.png" alt="img" />



<hr>

### 將 build 好的專案分別放進兩台 Tomcat 的 webapps 下


_以下為 myTomcat16 VM_
```
[vagrant@Tomcat16 bin]$ cp /vagrant_data/demo.war /usr/local/apache-tomcat-9.0.39/webapps/
[vagrant@Tomcat16 bin]$ cp -R /vagrant_data/demoWeb/ /usr/local/apache-tomcat-9.0.39/webapps/demoWeb
# /demoWeb/ => EmployeeTable
```
_以下為 myTomcat17 VM_
```
[vagrant@Tomcat17 bin]$ cp /vagrant_data/demo.war /usr/local/apache-tomcat-9.0.39/webapps/
[vagrant@Tomcat17 bin]$ cp -R /vagrant_data/demoWeb2/ /usr/local/apache-tomcat-9.0.39/webapps/demoWeb 
# /demoWeb2/ => EmployeeTable2222222222
```

可以看到 war 檔自動解開了
```
[vagrant@Tomcat16 bin]$ ll /usr/local/apache-tomcat-9.0.39/webapps/
total 36496
drwxr-x---.  5 vagrant vagrant       48 Nov 20 12:39 demo
-rwxrwxr-x.  1 vagrant vagrant 37361781 Nov 20 12:39 demo.war
drwxrwxr-x.  2 vagrant vagrant      239 Nov 20 12:39 demoWeb
drwxr-x---. 15 vagrant vagrant     4096 Nov 20 12:19 docs
drwxr-x---.  6 vagrant vagrant       83 Nov 20 12:19 examples
drwxr-x---.  5 vagrant vagrant       87 Nov 20 12:19 host-manager
drwxr-x---.  6 vagrant vagrant      114 Nov 20 12:19 manager
drwxr-x---.  3 vagrant vagrant     4096 Nov 20 12:19 ROOT
```

> 重啟 tomcat
>
> ```
> [vagrant@Tomcat16 bin]$ sh shutdown.sh # 需等幾秒再重啟(可用 sudo netstat -nlpt 查看 port 監聽狀況)
> [vagrant@Tomcat16 bin]$ sh startup.sh 
> ```

