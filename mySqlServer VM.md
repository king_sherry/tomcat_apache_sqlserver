
_以下為 mySqlServer VM_
### 安裝 SQLServer
**進到 CentOS_demo 目錄下，SSH 連線 mySqlServer**

```
PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant ssh mySqlServer
```


從網路上下載 SQLServer repository 到本地
```
[vagrant@SqlServer ~]$ sudo curl -o /etc/yum.repos.d/mssql-server.repo https://packages.microsoft.com/config/rhel/7/mssql-server-2019.repo
```

安裝 SQLServer 套件 (此步驟耗時較長)
```
[vagrant@SqlServer ~]$ sudo yum install -y mssql-server.x86_64
```

執行 SQLServer 安裝
```
[vagrant@SqlServer ~]$ sudo /opt/mssql/bin/mssql-conf setup
```

> **<span style="color: blue;">密碼設：Aa000000 (需有大小寫、數字)</span>**

<img src="./images/image-1.png" alt="img" />

查看 SqlServer 服務狀態
```
[vagrant@SqlServer ~]$ systemctl status mssql-server.service
```

開防火牆 port，SQLServer 預設 port 為 1433
```
[vagrant@SqlServer ~]$ sudo firewall-cmd --zone=public --add-port=1433/tcp --permanent
[vagrant@SqlServer ~]$ sudo firewall-cmd --reload
```

<br>

### 安裝 SQLServer 工具

從網路上下載 SQLServer tool repository 到本地
```
[vagrant@SqlServer ~]$ sudo curl -o /etc/yum.repos.d/msprod.repo https://packages.microsoft.com/config/rhel/7/prod.repo
```

安裝 SQLServer tool
```
[vagrant@SqlServer ~]$ sudo yum install -y mssql-tools.x86_64
```
加入 SQL 環境變數 **(加在 /etc/profile 裡面，既使重開機環境變數也會被重新讀取。且不管是哪個使用者登入時都會讀取該檔案)**
```
[vagrant@SqlServer ~]$ sudo vim /etc/profile
```
> 加上  
**<span style="color: blue;">export SQL_HOME=/opt/mssql-tools</span>**<br>
**<span style="color: blue;">export PATH=$SQL_HOME/bin:$PATH</span>**

在父程序執行設定檔，讓環境變數立即生效
```
[vagrant@SqlServer ~]$ source /etc/profile
```

> 查看 sql Command Line 指令  
[vagrant@SqlServer ~]$ sqlcmd -?

用剛剛的密碼和這台 VM 的 ip 位址連線資料庫 (連線成功會進入sql command)
```
[vagrant@SqlServer ~]$ sqlcmd -S 192.168.56.18 -U sa -P Aa000000
1> exit
[vagrant@SqlServer ~]$ 
```
> exit 跳出

<br>

### 還原資料庫

建立備份目錄 ( -p 參數可新增多層目錄 )
```
[vagrant@SqlServer ~]$ sudo mkdir -p /var/opt/mssql/backup
```

將備份檔案移至該目錄
```
[vagrant@SqlServer ~]$ sudo cp /vagrant_data/FACIAL_IDENTITY.bak /var/opt/mssql/backup
```

進入 sql command 進行 restore
```
[vagrant@SqlServer ~]$ sqlcmd -S 192.168.56.18 -U sa -P Aa000000
1> RESTORE DATABASE FACIAL_IDENTITY
2> FROM DISK = '/var/opt/mssql/backup/FACIAL_IDENTITY.bak'
3> WITH MOVE 'FACIAL_IDENTITY' TO '/var/opt/mssql/data/FACIAL_IDENTITY.mdf',
4> MOVE 'FACIAL_IDENTITY_log' TO '/var/opt/mssql/data/FACIAL_IDENTITY_log.ldf'
5> GO
```

_以下測試是否有還原成功_

查看 Databases

```
1> SELECT Name FROM sys.Databases
2> GO
```

查看 FACIAL_IDENTITY 的所有 tables
```
1> USE FACIAL_IDENTITY
2> SELECT * FROM INFORMATION_SCHEMA.TABLES
3> GO
```

select 一個 table
```
1> USE FACIAL_IDENTITY
2> SELECT * FROM EMPLOYEE
3> GO
```

