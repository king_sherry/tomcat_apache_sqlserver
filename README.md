**事前準備**

- [ ] [安裝 virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [ ] [安裝 Vagrant](https://www.vagrantup.com/downloads) (需設環境變數)

安裝完後查看 vagrant 版本，檢查是否安裝成功

```bash
> vagrant version
```

<hr>

### 建立四台VM(centos7)，一台裝 Apache Http、兩台裝 Tomcat、一台裝 SqlServer


> SqlServer 需要⾄少 2G 以上的記憶體
>
> ```ruby
> Vagrant.configure("2") do |config|
>   config.vm.box = "generic/centos7"
> 
>   # 將本機資料夾掛載至 VM 的/vagrant_data目錄下
>   config.vm.synced_folder "../VM_volumn", "/vagrant_data"
> 
>   # 準備安裝 Apache Http 的 VM
>   config.vm.define "myApache" do |apache|		# myApache：VM名稱 (ssh連線時要打的名稱)
>     apache.vm.hostname = "Apache"				# hostname：登入後終端機上顯示的名稱
>     apache.vm.network "private_network", ip: "192.168.56.15"	# 指定一個私有網路IP給虛擬機
>   end
> 
>   # 準備安裝 Tomcat 的兩台 VM
>   (16..17).each do |i|
>     config.vm.define "myTomcat#{i}" do |tomcat|
>       tomcat.vm.hostname = "Tomcat#{i}"
>       tomcat.vm.network "private_network", ip: "192.168.56.#{i}"
>     end
>   end
> 
>   # 準備安裝 SqlServer 的 VM
>   config.vm.define "mySqlServer" do |sql|
>     sql.vm.hostname = "SqlServer"
>     sql.vm.network "private_network", ip: "192.168.56.18"
>     sql.vm.provider "virtualbox" do |vb|
>       vb.memory = "4096"
>     end
>   end
> end
> ```

**先進到 CentOS_demo 目錄下(存放 Vagrantfile 的位置)** <span style="color: red;">(須注意路徑不得有中文名稱)</span>

可以用 vagrant validate 先檢查 Vagrantfile 有沒有錯

```
PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant validate
```

四台一起開機
```
PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant up
```
<img src="./images/image-0.png" alt="img" width="70%" />

> 四台一起關機
>
> ```
> PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant halt
> ```

[mySqlServer VM 操作步驟](./mySqlServer VM.md)

[myTomcat VM 操作步驟](./myTomcat VM.md)

[myApache VM 操作步驟](./myApache VM.md)

<hr>

_以下為 mySqlServer VM_
### 安裝 SQLServer

> [在 Ubuntu 上安裝 SQL Server](https://docs.microsoft.com/zh-tw/sql/linux/quickstart-install-connect-ubuntu?view=sql-server-ver15)

**進到 CentOS_demo 目錄下，SSH 連線 mySqlServer**

```
PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant ssh mySqlServer
```


從網路上下載 SQLServer repository 到本地
```
[vagrant@SqlServer ~]$ sudo curl -o /etc/yum.repos.d/mssql-server.repo https://packages.microsoft.com/config/rhel/7/mssql-server-2019.repo
```

安裝 SQLServer 套件 (此步驟耗時較長)
```
[vagrant@SqlServer ~]$ sudo yum install -y mssql-server.x86_64
```

執行 SQLServer 安裝
```
[vagrant@SqlServer ~]$ sudo /opt/mssql/bin/mssql-conf setup
```

> **<span style="color: blue;">密碼設：Aa000000 (需有大小寫、數字)</span>**

<img src="./images/image-1.png" alt="img" />

查看 SqlServer 服務狀態
```
[vagrant@SqlServer ~]$ systemctl status mssql-server.service
```

開防火牆 port，SQLServer 預設 port 為 1433
```
[vagrant@SqlServer ~]$ sudo firewall-cmd --zone=public --add-port=1433/tcp --permanent
[vagrant@SqlServer ~]$ sudo firewall-cmd --reload
```



### 安裝 SQLServer 工具

從網路上下載 SQLServer tool repository 到本地
```
[vagrant@SqlServer ~]$ sudo curl -o /etc/yum.repos.d/msprod.repo https://packages.microsoft.com/config/rhel/7/prod.repo
```

安裝 SQLServer tool
```
[vagrant@SqlServer ~]$ sudo yum install -y mssql-tools.x86_64
```
加入 SQL 環境變數 **(加在 /etc/profile 裡面，既使重開機環境變數也會被重新讀取。且不管是哪個使用者登入時都會讀取該檔案)**
```
[vagrant@SqlServer ~]$ sudo vim /etc/profile
```
> 加上  
**<span style="color: blue;">export SQL_HOME=/opt/mssql-tools</span>**<br>
**<span style="color: blue;">export PATH=$SQL_HOME/bin:$PATH</span>**

在父程序執行設定檔，讓環境變數立即生效
```
[vagrant@SqlServer ~]$ source /etc/profile
```

> 查看 sql Command Line 指令  
[vagrant@SqlServer ~]$ sqlcmd -?

用剛剛的密碼和這台 VM 的 ip 位址連線資料庫 (連線成功會進入sql command)
```
[vagrant@SqlServer ~]$ sqlcmd -S 192.168.56.18 -U sa -P Aa000000
1> exit
[vagrant@SqlServer ~]$ 
```
> exit 跳出



### 還原資料庫

建立備份目錄 ( -p 參數可新增多層目錄 )
```
[vagrant@SqlServer ~]$ sudo mkdir -p /var/opt/mssql/backup
```

將備份檔案移至該目錄
```
[vagrant@SqlServer ~]$ sudo cp /vagrant_data/FACIAL_IDENTITY.bak /var/opt/mssql/backup
```

進入 sql command 進行 restore
```
[vagrant@SqlServer ~]$ sqlcmd -S 192.168.56.18 -U sa -P Aa000000
1> RESTORE DATABASE FACIAL_IDENTITY
2> FROM DISK = '/var/opt/mssql/backup/FACIAL_IDENTITY.bak'
3> WITH MOVE 'FACIAL_IDENTITY' TO '/var/opt/mssql/data/FACIAL_IDENTITY.mdf',
4> MOVE 'FACIAL_IDENTITY_log' TO '/var/opt/mssql/data/FACIAL_IDENTITY_log.ldf'
5> GO
```

_以下測試是否有還原成功_

查看 Databases

```
1> SELECT Name FROM sys.Databases
2> GO
```

查看 FACIAL_IDENTITY 的所有 tables
```
1> USE FACIAL_IDENTITY
2> SELECT * FROM INFORMATION_SCHEMA.TABLES
3> GO
```

select 一個 table
```
1> USE FACIAL_IDENTITY
2> SELECT * FROM EMPLOYEE
3> GO
```



<hr>


_以下為 myTomcat VM (兩台都要做 )_
### 安裝 openjdk
**各自進到 CentOS_demo 目錄下，SSH 連線 myTomcat16 及 myTomcat17** (文件以 myTomcat16 操作示範)

```
PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant ssh myTomcat16
```

將掛進來的 tar.gz 檔解壓到 /tmp/ 資料夾下
```
[vagrant@Tomcat16 ~]$ tar xvzf /vagrant_data/openjdk-11+28_linux-x64_bin.tar.gz -C /tmp/
```

將 /jdk-11/ 移到 /usr/local/ 下
```
[vagrant@Tomcat16 ~]$ sudo mv /tmp/jdk-11/ /usr/local/
```
在 /etc/profile 設定 java 環境變數
```
[vagrant@Tomcat16 ~]$ sudo vim /etc/profile
```
> 加上  
**<span style="color: blue;">export JAVA_HOME=/usr/local/jdk-11</span>**<br>
**<span style="color: blue;">export PATH=$JAVA_HOME/bin:$PATH</span>**

在父程序執行設定檔，讓環境變數立即生效
```
[vagrant@Tomcat16 ~]$ source /etc/profile
```

檢查 java 版本
```
[vagrant@Tomcat16 ~]$ java -version
```


### 安裝 tomcat

將掛進來的 tar.gz 檔解壓到 /tmp/ 資料夾下
```
[vagrant@Tomcat16 ~]$ tar xvzf /vagrant_data/apache-tomcat-9.0.39.tar.gz -C /tmp/
```

將 apache-tomcat-9.0.39 移到 /usr/local/ 下
```
[vagrant@Tomcat16 ~]$ sudo mv /tmp/apache-tomcat-9.0.39/ /usr/local/
```

在 /etc/profile 設定 tomcat 環境變數
```
[vagrant@Tomcat16 ~]$ sudo vim /etc/profile
```
> 加上  
**<span style="color: blue;">export CATALINA_HOME=/usr/local/apache-tomcat-9.0.39</span>**

在父程序執行設定檔，讓環境變數立即生效
```
[vagrant@Tomcat16 ~]$ source /etc/profile
```

啟動 tomcat
```
[vagrant@Tomcat16 ~]$ cd /usr/local/apache-tomcat-9.0.39/bin/
[vagrant@Tomcat16 bin]$ sh startup.sh 
```
> 以下指令可查看 tcp 協定的 port 監聽狀態  
[vagrant@Tomcat bin]$ sudo netstat -nlpt<br><br>_8080 --默認的 http 監聽 port_  
_8005 --用來關閉 tomcat 服務的 port_

測試
```
[vagrant@Tomcat16 bin]$ curl localhost:8080
或
[vagrant@Tomcat16 bin]$ curl 192.168.56.16:8080
```

將防火牆的對應端口打開
```
[vagrant@Tomcat16 bin]$ sudo firewall-cmd --zone=public --permanent --add-port=8080/tcp
[vagrant@Tomcat16 bin]$ sudo firewall-cmd --reload
```
用 ip 192.168.56.16:8080 在 windows 主機測試 http://192.168.56.16:8080  ，若成功會看到以下頁面  

<img src="./images/image-2.png" alt="img" />



<hr>


_以下為 myApache VM_
### 安裝 Httpd
**進到 CentOS_demo 目錄下，SSH 連線 myApache**

```
PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant ssh myApache 
```

安裝 httpd
```
[vagrant@Apache ~]$ sudo yum install -y httpd
```

查看 httpd 版本
```
[vagrant@Apache ~]$ httpd -v
```
啟動 httpd.service
```
[vagrant@Apache ~]$ sudo systemctl start httpd.service
# enable 設定開機自動啟動服務
[vagrant@Apache ~]$ sudo systemctl enable httpd.service
```
查看 httpd 服務狀態 

```
[vagrant@Apache ~]$ systemctl status httpd.service
```

測試
```
[vagrant@Apache ~]$ curl 192.168.56.15
```

將防火牆的 http(80) 對應端口打開
```
[vagrant@Apache ~]$ sudo firewall-cmd --zone=public --permanent --add-port=80/tcp
[vagrant@Apache ~]$ sudo firewall-cmd --reload
```

用 ip 192.168.56.15 在 windows 主機測試 http://192.168.56.15 ，若成功會看到以下頁面  
<img src="./images/image-3.png" alt="img" />



### 加上 SSL 憑證

新建一個 ssl 資料夾
```
[vagrant@Apache ~]$ sudo mkdir /etc/httpd/ssl
```

進到該資料夾下後用 openssl 建立 private key 及 CSR(憑證簽署要求) 檔案  (會要求輸入國家、城市、機構名稱及 domain name 等資訊)
```
[vagrant@Apache ~]$ cd /etc/httpd/ssl/
[vagrant@Apache ssl]$ sudo openssl req -nodes -newkey rsa:2048 -keyout myserver.key -out server.csr
```
<img src="./images/image-4.png" alt="img" />

> [vagrant@Apache ssl]$ ls -l  
> total 8  
> -rw-r--r--. 1 root root 1708 Nov 17 04:31 myserver.key  
> -rw-r--r--. 1 root root  989 Nov 17 04:31 server.csr

用 openssl 建立自簽憑證 ca.crt 檔案
```
[vagrant@Apache ssl]$ sudo openssl x509 -req -days 365 -in server.csr -signkey myserver.key -out ca.crt
```
> Signature ok  
subject=/C=TW/ST=Taiwan/L=Taipei/O=fstop/CN=www.sherrychin.com  
Getting Private key<br><br>
[vagrant@Apache ssl]$ ls -l  
total 12  
-rw-r--r--. 1 root root 1172 Nov 17 04:34 ca.crt  
-rw-r--r--. 1 root root 1708 Nov 17 04:31 myserver.key  
-rw-r--r--. 1 root root  989 Nov 17 04:31 server.csr

安裝 mod_ssl 模組 (才會產生 ssl.conf 設定檔)
```
[vagrant@Apache ~]$ sudo yum install -y mod_ssl
```

找出 SSL 設定檔
```
[vagrant@Apache ssl]$ grep -i -r "SSLCertificateFile" /etc/httpd/
```

> -i：忽略大小寫  
>
> -r：搜尋指定的關鍵字

vim 進入編輯 (記得看有沒有原本就有的參數)

```
[vagrant@Apache ssl]$ sudo vim /etc/httpd/conf.d/ssl.conf
```
> 確保有這幾行設定：  
**<span style="color: blue;">ServerName www.sherrychin.com:443	#(產生憑證時設的網域名稱)</span>**<br>
**<span style="color: blue;">SSLEngine on</span>**<br>
**<span style="color: blue;">SSLProtocol all -SSLv2 -SSLv3	# 關閉不安全的通訊協定 SSLv2 SSLv3</span>**<br>
**<span style="color: blue;">SSLCertificateFile /etc/httpd/ssl/ca.crt	#(伺服器憑證路徑)</span>**<br>
**<span style="color: blue;">SSLCertificateKeyFile /etc/httpd/ssl/myserver.key	#(憑證私鑰路徑)</span>**

> **vim**  
[ESC] (離開編輯模式) + /關鍵字 + [ENTER]：找關鍵字，按 n 繼續找下一個關鍵字  
i：進入編輯(insert)  
[ESC] (離開編輯模式) + :wq + [ENTER]：存檔並關閉

重新啟動 httpd.service
```
[vagrant@Apache ssl]$ sudo systemctl restart httpd.service
```

剛剛的設定檔內提到 HTTPS 監聽的 port 是 443，將防火牆對應端口打開
```
[vagrant@Apache ssl]$ sudo firewall-cmd --zone=public --permanent --add-port=443/tcp
[vagrant@Apache ssl]$ sudo firewall-cmd --reload
```
用 **https** 加 ip 192.168.56.15 在 windows 主機測試 https://192.168.56.15/ ，若成功會看到和剛剛一樣的頁面 



> 由於上面只是使用自簽憑證，瀏覽器不能驗證憑證，所以會發出警告憑證不可信，只要選擇繼續瀏覽，便可以看到加密了的網頁。



用系統管理員的身分更改 windows 主機 hosts 設定檔 (作用跟 DNS 伺服器相同)

> 加上  
192.168.56.15 www.sherrychin.com

在 windows 主機測試 https://www.sherrychin.com/ ，若成功會看到和剛剛一樣的頁面



<hr>


### Apache Tomcat Connect
> 先檢查有沒有導入必要模組
```
[vagrant@Apache ssl]$ grep -i -r 'mod_proxy.so' /etc/httpd/
/etc/httpd/conf.modules.d/00-proxy.conf:LoadModule proxy_module modules/mod_proxy.so
[vagrant@Apache ssl]$ grep -i -r 'mod_proxy_http.so' /etc/httpd/
/etc/httpd/conf.modules.d/00-proxy.conf:LoadModule proxy_http_module modules/mod_proxy_http.so
[vagrant@Apache ssl]$ grep -i -r 'mod_proxy_balancer.so' /etc/httpd/
/etc/httpd/conf.modules.d/00-proxy.conf:LoadModule proxy_balancer_module modules/mod_proxy_balancer.so
```
在 /etc/httpd/conf/httpd.conf 設定檔加上 proxy 設定

```
[vagrant@Apache ssl]$ sudo vim /etc/httpd/conf/httpd.conf
```
> 加上這幾行設定：  
**<span style="color: blue;"><Proxy balancer://myapp></span>**<br>
**<span style="color: blue;">Order deny,allow</span>**<br>
**<span style="color: blue;">Allow from all</span>**<br>
**<span style="color: blue;">BalancerMember http://192.168.56.16:8080 loadfactor=8</span>**<br>
**<span style="color: blue;">BalancerMember http://192.168.56.17:8080 loadfactor=2</span>**<br>
**<span style="color: blue;">\</Proxy></span>**<br>
**<span style="color: blue;">ProxyPass / balancer://myapp/</span>**<br>
**<span style="color: blue;">ProxyPassReverse / balancer://myapp/</span>**<br>
**<span style="color: blue;">ProxyRequests Off</span>**

> *ProxyRequests Off*
>
> ​	因為只需要反向代理，不需要開啟 ProxyRequests 功能

> _loadfactor_ 
>
> ​	負載分配權重

> Order deny,allow
>
> Allow from all
>
> ​	若有使用正向代理 ( ProxyRequests 配置 )，不限制訪問權限的話，任何 client 端都可以使用這台proxy server來訪問任意主機，	同時隱藏其真實身份。
>
> ​	使用反向代理時 (ProxyPass 配置)，與 ProxyRequests Off 一起使用，訪問控制的重要性就比較低，因為客戶端只能連到專門配置	的主機。

rhel7/centos 會因為 SELinux 默認配置而使代理無法訪問網頁，所以要先將 httpd_can_network_connect 設定開啟
```
[vagrant@Apache ssl]$ getsebool httpd_can_network_connect
httpd_can_network_connect --> off
[vagrant@Apache ssl]$ sudo /usr/sbin/setsebool -P httpd_can_network_connect=1
[vagrant@Apache ssl]$ getsebool httpd_can_network_connect
httpd_can_network_connect --> on
```

重啟 Apache
```
[vagrant@Apache ssl]$ sudo systemctl restart httpd.service
```



<hr>


將 build 好的專案分別放進兩台 Tomcat 的 webapps 下

_以下為 myTomcat16 VM_
```
[vagrant@Tomcat16 bin]$ cp /vagrant_data/demo.war /usr/local/apache-tomcat-9.0.39/webapps/
[vagrant@Tomcat16 bin]$ cp -R /vagrant_data/demoWeb/ /usr/local/apache-tomcat-9.0.39/webapps/demoWeb
# /demoWeb/ => EmployeeTable
```
_以下為 myTomcat17 VM_
```
[vagrant@Tomcat17 bin]$ cp /vagrant_data/demo.war /usr/local/apache-tomcat-9.0.39/webapps/
[vagrant@Tomcat17 bin]$ cp -R /vagrant_data/demoWeb2/ /usr/local/apache-tomcat-9.0.39/webapps/demoWeb 
# /demoWeb2/ => EmployeeTable2222222222
```

可以看到 war 檔自動解開了
```
[vagrant@Tomcat16 bin]$ ll /usr/local/apache-tomcat-9.0.39/webapps/
total 36496
drwxr-x---.  5 vagrant vagrant       48 Nov 20 12:39 demo
-rwxrwxr-x.  1 vagrant vagrant 37361781 Nov 20 12:39 demo.war
drwxrwxr-x.  2 vagrant vagrant      239 Nov 20 12:39 demoWeb
drwxr-x---. 15 vagrant vagrant     4096 Nov 20 12:19 docs
drwxr-x---.  6 vagrant vagrant       83 Nov 20 12:19 examples
drwxr-x---.  5 vagrant vagrant       87 Nov 20 12:19 host-manager
drwxr-x---.  6 vagrant vagrant      114 Nov 20 12:19 manager
drwxr-x---.  3 vagrant vagrant     4096 Nov 20 12:19 ROOT
```

> 重啟 tomcat
>
> ```
> [vagrant@Tomcat16 bin]$ sh shutdown.sh # 需等幾秒再重啟(可用 sudo netstat -nlpt 查看 port 監聽狀況)
> [vagrant@Tomcat16 bin]$ sh startup.sh 
> ```

_測試_  
https://192.168.56.15/demoWeb/  
或  
https://www.sherrychin.com/demoWeb/

若成功會看到以下頁面 (理論上根據剛剛設置的 load balance，10 次會有 8 次是 EmployeeTable，2 次是 EmployeeTable2222222222) (建議使用無痕分頁測試)  
<img src="./images/image-5.png" alt="img" />



可以試著改寫資料庫測試

_以下為 mySqlServer VM_
```
1> USE FACIAL_IDENTITY
2> INSERT INTO EMPLOYEE VALUES('000000', 'sherry', '888', 'java', 'sherrychin@fstop.com.tw', '20201120');
3> GO
```
新增後按下最底下的更新按紐  

<img src="./images/image-6.png" alt="img" />

可以看到剛剛新增的一筆資料  

<img src="./images/image-7.png" alt="img" />