### 安裝 Apache2

進到有 Vagrantfile 的目錄下 ssh 連線 VM
```
PS C:\Users\fstop\Desktop\test\ubuntu_ssl> cd .\ubuntuVM\
PS C:\Users\fstop\Desktop\test\ubuntu_ssl\ubuntuVM> vagrant ssh myApache
```

安裝 apache2
```
vagrant@Apache:~$ sudo apt-get update 
vagrant@Apache:~$ sudo apt-get install -y apache2
```

查看 apache2 版本
```
vagrant@Apache:~$ apache2 -v
```

查看 apache2 服務狀態 
```
vagrant@Apache:~$ sudo systemctl status apache2
```

測試
```
vagrant@Apache:~$ curl 192.168.56.15
```
**ubuntu 防火牆預設關閉**

在 windows 主機測試 http://192.168.56.15 ，若成功會看到以下頁面  
<img src="./images/image-8.png" alt="img" style="width: 500px" />

<br>

### 加上 SSL 憑證

新建一個 ssl 資料夾
```
vagrant@Apache:~$ sudo mkdir /etc/apache2/ssl
```

進到該資料夾下後用 openssl 建立 private key 及 CSR(憑證簽署要求) 檔案  
```
vagrant@Apache:~$ cd /etc/apache2/ssl
vagrant@Apache:/etc/apache2/ssl$ sudo openssl req -nodes -newkey rsa:2048 -keyout myserver.key -out server.csr
```

用 openssl 建立自簽憑證 ca.crt 檔案
```
vagrant@Apache:/etc/apache2/ssl$ sudo openssl x509 -req -days 365 -in server.csr -signkey myserver.key -out ca.crt
```

啟用 apache ssl 模組
```
vagrant@Apache:/etc/apache2/ssl$ sudo a2enmod ssl

# 重啟 apache2
vagrant@Apache:/etc/apache2/ssl$ sudo service apache2 restart
```

找出 SSL 設定檔
```
vagrant@Apache:/etc/apache2/ssl$ grep -ir 'SSLCertificateFile' /etc/apache2/
```
> -i：忽略大小寫  
> -r：搜尋指定的關鍵字

vim 進入編輯 **(ubuntu 預設一進入就是 replace 編輯模式)**  
> **vim**  
[ESC] (離開編輯模式) + /關鍵字 + [ENTER]：找關鍵字，按 n 繼續找下一個關鍵字  
i：進入編輯(insert)  
[ESC] (離開編輯模式) + :wq + [ENTER]：存檔並關閉
```
vagrant@Apache:/etc/apache2/ssl$ sudo vim /etc/apache2/sites-available/default-ssl.conf
```
> 確保有以下設定<br>
**<span style="color: blue;">SSLEngine on</span>**<br>
**<span style="color: blue;">SSLCertificateFile /etc/apache2/ssl/ca.crt	#(伺服器憑證路徑)</span>**<br>
**<span style="color: blue;">SSLCertificateKeyFile /etc/apache2/ssl/myserver.key	#(憑證私鑰路徑)</span>**<br>
**<span style="color: blue;">ServerName www.sherrychin.com:443</span>**

沒有找到 SSLProtocol，發現在其他檔案中
```
vagrant@Apache:/etc/apache2/ssl$ grep -ir 'SSLProtocol' /etc/apache2/
```
```
vagrant@Apache:/etc/apache2/ssl$ sudo vim /etc/apache2/mods-available/ssl.conf
```
> 確保有以下設定<br>
**<span style="color: blue;">SSLProtocol all -SSLv2 -SSLv3</span>**

重新啟動 SSL-enabled虛擬主機
```
vagrant@Apache:/etc/apache2/ssl$ sudo a2ensite default-ssl.conf
```

重新啟動 apache2
```
vagrant@Apache:/etc/apache2/ssl$ sudo service apache2 restart
```

在 windows 主機測試 https://192.168.56.15/ ，若成功會看到和剛剛一樣的頁面

> 由於上面只是使用自簽憑證，瀏覽器不能驗證憑證，所以會發出警告憑證不可信，只要選擇繼續瀏覽，便可以看到加密了的網頁。

用系統管理員的身分更改 windows 主機 hosts 設定檔 (作用跟 DNS 伺服器相同)

> 加上  
192.168.56.15 www.sherrychin.com

在 windows 主機測試 https://www.sherrychin.com/ ，若成功會看到和剛剛一樣的頁面

<hr>
<br>

### Apache Tomcat Connect
> 先啟動必要模組
```
vagrant@Apache:/etc/apache2/ssl$ sudo a2enmod proxy
vagrant@Apache:/etc/apache2/ssl$ sudo a2enmod proxy_http
vagrant@Apache:/etc/apache2/ssl$ sudo a2enmod proxy_balancer
vagrant@Apache:/etc/apache2/ssl$ sudo a2enmod lbmethod_byrequests
```

在 /etc/apache2/sites-available/000-default.conf 設定檔加上 proxy 設定
```
vagrant@Apache:/etc/apache2/ssl$ sudo vim /etc/apache2/sites-available/000-default.conf
```
> 加上這幾行設定：<br><br>
**<span style="color: blue;"><Proxy balancer://myapp></span>**<br>
**<span style="color: blue;">Order deny,allow</span>**<br>
**<span style="color: blue;">Allow from all</span>**<br>
**<span style="color: blue;">BalancerMember http://192.168.56.16:8080 loadfactor=8</span>**<br>
**<span style="color: blue;">BalancerMember http://192.168.56.17:8080 loadfactor=2</span>**<br>
**<span style="color: blue;">\</Proxy></span>**<br>
**<span style="color: blue;">ProxyPass / balancer://myapp/</span>**<br>
**<span style="color: blue;">ProxyPassReverse / balancer://myapp/</span>**<br>
**<span style="color: blue;">ProxyRequests Off</span>**

重啟 Apache2
```
vagrant@Apache:/etc/apache2/ssl$ sudo service apache2 restart
```

<hr>

#### 測試

https://192.168.56.15/demoWeb/  
或  
https://www.sherrychin.com/demoWeb/

若成功會看到以下頁面 (理論上根據剛剛設置的 load balance，10 次會有 8 次是 EmployeeTable，2 次是 EmployeeTable2222222222) (建議使用無痕分頁測試)  
<img src="./images/image-5.png" alt="img" />
