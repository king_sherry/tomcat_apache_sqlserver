_以下為 myApache VM_
### 安裝 Httpd
**進到 CentOS_demo 目錄下，SSH 連線 myApache**

```
PS D:\tomcat_apache_sqlServer\CentOS_demo> vagrant ssh myApache 
```

安裝 httpd
```
[vagrant@Apache ~]$ sudo yum install -y httpd
```

查看 httpd 版本
```
[vagrant@Apache ~]$ httpd -v
```
啟動 httpd.service
```
[vagrant@Apache ~]$ sudo systemctl start httpd.service
# enable 設定開機自動啟動服務
[vagrant@Apache ~]$ sudo systemctl enable httpd.service
```
查看 httpd 服務狀態 

```
[vagrant@Apache ~]$ systemctl status httpd.service
```

測試
```
[vagrant@Apache ~]$ curl 192.168.56.15
```

將防火牆的 http(80) 對應端口打開
```
[vagrant@Apache ~]$ sudo firewall-cmd --zone=public --permanent --add-port=80/tcp
[vagrant@Apache ~]$ sudo firewall-cmd --reload
```

用 ip 192.168.56.15 在 windows 主機測試 http://192.168.56.15 ，若成功會看到以下頁面  
<img src="./images/image-3.png" alt="img" />

<br>

### 加上 SSL 憑證

新建一個 ssl 資料夾
```
[vagrant@Apache ~]$ sudo mkdir /etc/httpd/ssl
```

進到該資料夾下後用 openssl 建立 private key 及 CSR(憑證簽署要求) 檔案  (會要求輸入國家、城市、機構名稱及 domain name 等資訊)
```
[vagrant@Apache ~]$ cd /etc/httpd/ssl/
[vagrant@Apache ssl]$ sudo openssl req -nodes -newkey rsa:2048 -keyout myserver.key -out server.csr
```
<img src="./images/image-4.png" alt="img" />

> [vagrant@Apache ssl]$ ls -l  
> total 8  
> -rw-r--r--. 1 root root 1708 Nov 17 04:31 myserver.key  
> -rw-r--r--. 1 root root  989 Nov 17 04:31 server.csr

用 openssl 建立自簽憑證 ca.crt 檔案
```
[vagrant@Apache ssl]$ sudo openssl x509 -req -days 365 -in server.csr -signkey myserver.key -out ca.crt
```
> Signature ok  
subject=/C=TW/ST=Taiwan/L=Taipei/O=fstop/CN=www.sherrychin.com  
Getting Private key<br><br>
[vagrant@Apache ssl]$ ls -l  
total 12  
-rw-r--r--. 1 root root 1172 Nov 17 04:34 ca.crt  
-rw-r--r--. 1 root root 1708 Nov 17 04:31 myserver.key  
-rw-r--r--. 1 root root  989 Nov 17 04:31 server.csr

安裝 mod_ssl 模組 (才會產生 ssl.conf 設定檔)
```
[vagrant@Apache ~]$ sudo yum install -y mod_ssl
```

找出 SSL 設定檔
```
[vagrant@Apache ssl]$ grep -i -r "SSLCertificateFile" /etc/httpd/
```

> -i：忽略大小寫  
>
> -r：搜尋指定的關鍵字

vim 進入編輯 (記得看有沒有原本就有的參數)

```
[vagrant@Apache ssl]$ sudo vim /etc/httpd/conf.d/ssl.conf
```
> 確保有這幾行設定：  
**<span style="color: blue;">ServerName www.sherrychin.com:443	#(產生憑證時設的網域名稱)</span>**<br>
**<span style="color: blue;">SSLEngine on</span>**<br>
**<span style="color: blue;">SSLProtocol all -SSLv2 -SSLv3	# 關閉不安全的通訊協定 SSLv2 SSLv3</span>**<br>
**<span style="color: blue;">SSLCertificateFile /etc/httpd/ssl/ca.crt	#(伺服器憑證路徑)</span>**<br>
**<span style="color: blue;">SSLCertificateKeyFile /etc/httpd/ssl/myserver.key	#(憑證私鑰路徑)</span>**

> **vim**  
[ESC] (離開編輯模式) + /關鍵字 + [ENTER]：找關鍵字，按 n 繼續找下一個關鍵字  
i：進入編輯(insert)  
[ESC] (離開編輯模式) + :wq + [ENTER]：存檔並關閉

重新啟動 httpd.service
```
[vagrant@Apache ssl]$ sudo systemctl restart httpd.service
```

剛剛的設定檔內提到 HTTPS 監聽的 port 是 443，將防火牆對應端口打開
```
[vagrant@Apache ssl]$ sudo firewall-cmd --zone=public --permanent --add-port=443/tcp
[vagrant@Apache ssl]$ sudo firewall-cmd --reload
```
用 **https** 加 ip 192.168.56.15 在 windows 主機測試 https://192.168.56.15/ ，若成功會看到和剛剛一樣的頁面 



> 由於上面只是使用自簽憑證，瀏覽器不能驗證憑證，所以會發出警告憑證不可信，只要選擇繼續瀏覽，便可以看到加密了的網頁。



用系統管理員的身分更改 windows 主機 hosts 設定檔 (作用跟 DNS 伺服器相同)

> 加上  
192.168.56.15 www.sherrychin.com

在 windows 主機測試 https://www.sherrychin.com/ ，若成功會看到和剛剛一樣的頁面



<hr>
<br>

### Apache Tomcat Connect
> 先檢查有沒有導入必要模組
```
[vagrant@Apache ssl]$ grep -i -r 'mod_proxy.so' /etc/httpd/
/etc/httpd/conf.modules.d/00-proxy.conf:LoadModule proxy_module modules/mod_proxy.so
[vagrant@Apache ssl]$ grep -i -r 'mod_proxy_http.so' /etc/httpd/
/etc/httpd/conf.modules.d/00-proxy.conf:LoadModule proxy_http_module modules/mod_proxy_http.so
[vagrant@Apache ssl]$ grep -i -r 'mod_proxy_balancer.so' /etc/httpd/
/etc/httpd/conf.modules.d/00-proxy.conf:LoadModule proxy_balancer_module modules/mod_proxy_balancer.so
```
在 /etc/httpd/conf/httpd.conf 設定檔加上 proxy 設定

```
[vagrant@Apache ssl]$ sudo vim /etc/httpd/conf/httpd.conf
```
> 加上這幾行設定：  
**<span style="color: blue;"><Proxy balancer://myapp></span>**<br>
**<span style="color: blue;">Order deny,allow</span>**<br>
**<span style="color: blue;">Allow from all</span>**<br>
**<span style="color: blue;">BalancerMember http://192.168.56.16:8080 loadfactor=8</span>**<br>
**<span style="color: blue;">BalancerMember http://192.168.56.17:8080 loadfactor=2</span>**<br>
**<span style="color: blue;">\</Proxy></span>**<br>
**<span style="color: blue;">ProxyPass / balancer://myapp/</span>**<br>
**<span style="color: blue;">ProxyPassReverse / balancer://myapp/</span>**<br>
**<span style="color: blue;">ProxyRequests Off</span>**

> *ProxyRequests Off*
>
> ​	因為只需要反向代理，不需要開啟 ProxyRequests 功能

> _loadfactor_ 
>
> ​	負載分配權重

> Order deny,allow
>
> Allow from all
>
> ​	若有使用正向代理 ( ProxyRequests 配置 )，不限制訪問權限的話，任何 client 端都可以使用這台proxy server來訪問任意主機，	同時隱藏其真實身份。
>
> ​	使用反向代理時 (ProxyPass 配置)，與 ProxyRequests Off 一起使用，訪問控制的重要性就比較低，因為客戶端只能連到專門配置	的主機。

rhel7/centos 會因為 SELinux 默認配置而使代理無法訪問網頁，所以要先將 httpd_can_network_connect 設定開啟
```
[vagrant@Apache ssl]$ getsebool httpd_can_network_connect
httpd_can_network_connect --> off
[vagrant@Apache ssl]$ sudo /usr/sbin/setsebool -P httpd_can_network_connect=1
[vagrant@Apache ssl]$ getsebool httpd_can_network_connect
httpd_can_network_connect --> on
```

重啟 Apache
```
[vagrant@Apache ssl]$ sudo systemctl restart httpd.service
```

<br>

#### 測試

https://192.168.56.15/demoWeb/  
或  
https://www.sherrychin.com/demoWeb/

若成功會看到以下頁面 (理論上根據剛剛設置的 load balance，10 次會有 8 次是 EmployeeTable，2 次是 EmployeeTable2222222222) (建議使用無痕分頁測試)  
<img src="./images/image-5.png" alt="img" />



可以試著改寫資料庫測試

_以下為 mySqlServer VM_
```
1> USE FACIAL_IDENTITY
2> INSERT INTO EMPLOYEE VALUES('000000', 'sherry', '888', 'java', 'sherrychin@fstop.com.tw', '20201120');
3> GO
```
新增後按下最底下的更新按紐  

<img src="./images/image-6.png" alt="img" />

可以看到剛剛新增的一筆資料  

<img src="./images/image-7.png" alt="img" />