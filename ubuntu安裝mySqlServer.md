Vagrantfile box 改成 ubuntu1604
```ruby
config.vm.box = "generic/ubuntu1604"
```

### 在 Ubuntu 上安裝 SQL Server
[Microsoft 安裝教學文件](https://docs.microsoft.com/zh-tw/sql/linux/quickstart-install-connect-ubuntu?view=sql-server-ver15)

進到有 Vagrantfile 的目錄下 ssh 連線 VM
```
PS C:\Users\fstop\Desktop\test\ubuntu_ssl> cd .\ubuntuVM\
PS C:\Users\fstop\Desktop\test\ubuntu_ssl\ubuntuVM> vagrant ssh mySqlServer
```
匯入公開存放庫 GPG 金鑰
```
vagrant@SqlServer:~$ wget -qO- https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
```

為 SQL Server 2019 註冊 Microsoft SQL Server Ubuntu 存放庫(Ubuntu 16.04)
```
vagrant@SqlServer:~$ sudo add-apt-repository "$(wget -qO- https://packages.microsoft.com/config/ubuntu/16.04/mssql-server-2019.list)"
```

安裝 SQLServer 套件
```
vagrant@SqlServer:~$ sudo apt-get update
vagrant@SqlServer:~$ sudo apt-get install -y mssql-server
```

執行 SQLServer 安裝
```
vagrant@SqlServer:~$ sudo /opt/mssql/bin/mssql-conf setup
```

查看 SqlServer 服務狀態
```
vagrant@SqlServer:~$ systemctl status mssql-server --no-pager
```
> **ubuntu 防火牆預設關閉**  
`vagrant@SqlServer:~$ sudo ufw status`  
Status: inactive


<br>

### 安裝 SQLServer 工具

匯入公開存放庫 GPG 金鑰
```
vagrant@SqlServer:~$ curl https://packages.microsoft.com/keys/microsoft.asc | sudo apt-key add -
```

註冊 Microsoft Ubuntu 存放庫(Ubuntu 16.04)
```
vagrant@SqlServer:~$ curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list | sudo tee /etc/apt/sources.list.d/msprod.list
```

安裝 SQLServer tool
```
vagrant@SqlServer:~$ sudo apt-get update
vagrant@SqlServer:~$ sudo apt-get install mssql-tools
```
加入 SQL 環境變數 **(加在 /etc/profile 裡面，既使重開機環境變數也會被重新讀取。且不管是哪個使用者登入時都會讀取該檔案)**
```
vagrant@SqlServer:~$ sudo vim /etc/profile
```
> vim 進入編輯 **(ubuntu 預設一進入就是 replace 編輯模式)**  
> 加上  
**<span style="color: blue;">export SQL_HOME=/opt/mssql-tools</span>**<br>
**<span style="color: blue;">export PATH=$SQL_HOME/bin:$PATH</span>**

在父程序執行設定檔，讓環境變數立即生效
```
vagrant@SqlServer:~$ source /etc/profile
```

> 查看 sql Command Line 指令  
vagrant@SqlServer:~$ sqlcmd -?

用剛剛的密碼和這台 VM 的 ip 位址連線資料庫 (連線成功會進入sql command)
```
vagrant@SqlServer:~$ sqlcmd -S 192.168.56.18 -U sa -P Aa000000
1> exit
vagrant@SqlServer:~$ 
```
> exit 跳出
